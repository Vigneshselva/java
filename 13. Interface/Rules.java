public interface Rules 
{
	int no_Of_Leaves = 15; // interface variable is default final and static
	int minSalary = 15000; // interface variable is default final and static
	
	public void comeOnTime(); // Interface method is default abstract
	
	public void takeLeave(); // Interface method is default abstract
	
	public void setSalary(); // Interface method is default abstract
}
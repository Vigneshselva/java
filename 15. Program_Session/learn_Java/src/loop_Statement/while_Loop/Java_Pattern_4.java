package loop_Statement.while_Loop;

//output
//1 2 3 4 5
//6 7 8 9 10
//11 12 13 14 15
//16 17 18 19 20

public class Java_Pattern_4 
{

	public static void main(String[] args) 
	{
		int no = 1;
		while (no<=20)
		{
			
			System.out.print(no + " ");
			if(no % 5 == 0)
			{
				System.out.println();
			}
			no = no + 1;
		}

	}

}

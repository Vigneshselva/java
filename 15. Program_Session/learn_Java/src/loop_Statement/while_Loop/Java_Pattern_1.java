package loop_Statement.while_Loop;

//output
//0 0 0 0 0
//1 1 1 1 1 
//2 2 2 2 2 
//3 3 3 3 3 
//4 4 4 4 4
//5 5 5 5 5

public class Java_Pattern_1 
{

	public static void main(String[] args) 
	{
		int row = 0;
		
		while(row <= 5)
		{
			int col = 1;
			while(col<=5)
			{
				System.out.print(row + " ");
				col = col + 1;
			}
			row = row + 1;
			System.out.println();
		}

	}

}

package loop_Statement.while_Loop;

//output
//5 4 3 2 1
//5 4 3 2 1
//5 4 3 2 1
//5 4 3 2 1
//5 4 3 2 1


public class Java_Pattern_3 
{

	public static void main(String[] args) 
	{
		int row = 1;
		
		while (row <= 5)
		{
			int col = 5;
			while(col >= 1)
			{
				System.out.print(col + " ");
				col = col - 1;
			}
			System.out.println();
			row = row + 1;
		}

	}

}

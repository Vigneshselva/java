package loop_Statement.while_Loop.Task;

//output
//A A A A A
//B B B B B
//C C C C C
//D D D D D

public class Task_5 
{
//output
//A A A A A
//B B B B B
//C C C C C
//D D D D D

	public static void main(String[] args) 
	{
		int row = 65;
		while(row <= 68)
		{
			int col = 1;
			while(col <= 5)
			{
				System.out.print( (char)row  + " ");
				col = col + 1;
			}
			row = row + 1;
			System.out.println();
		}

	}

}

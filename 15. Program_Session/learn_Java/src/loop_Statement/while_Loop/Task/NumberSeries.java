package loop_Statement.while_Loop.Task;

public class NumberSeries 
{
	// 14,19,29,49,89,169,329 
	public static void main(String[] args) 
	{
		int firstNumber = 14;
		
		System.out.println(firstNumber);
		 
		int no = 0;
		
		int temp = 1;
		
		while(no < 6)
		{
			
			firstNumber = firstNumber+5*temp;
			temp = temp * 2;
			System.out.println(firstNumber);
			no++;
		}
	}

}

package loop_Statement.while_Loop.Task;

// output
//1 2 3 4 5

public class Task_3 
{

	public static void main(String[] args) 
	{
		int count =1 ;
		
		while (count <= 5)
		{
			System.out.print(count + " ");
			count = count + 1;
		}
	}

}

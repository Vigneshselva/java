package array.Task;

public class ThreeDimensionalArrayTask 
{

	public static void main(String[] args) 
	{
		int[][][] marks = 	{{{90,25,89,10,18},
				  			{45,25,89,100,34},
				  			{90,35,89,70,58}},
							{{90,25,89,10,18},
			  				{45,25,89,100,34},
			  				{90,35,89,70,58}}};
		
		//System.out.println(marks.length);
		
		
		
		for(int arrayNo = 0; arrayNo < marks.length;arrayNo++)
		{
			for(int row = 0;row < marks[arrayNo].length;row++)
			{
				for(int col = 0;col < marks[arrayNo][row].length;col++)
				{
					System.out.print(marks[arrayNo][row][col]+" ");
				}
				System.out.println();
			}
		}

	}

}

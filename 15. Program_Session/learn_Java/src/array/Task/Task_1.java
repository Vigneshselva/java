package array.Task;

public class Task_1 
{

	public static void main(String[] args) 
	{
		int[] marks = {98,87,97,98,89};
		
		int total = 0;
		int average;
		int max = 0;
		int min = marks[0];
		int failCount = 0;
		
		for(int index = 0;index < marks.length;index++)
		{
//			System.out.println(marks[index]);
			if(marks[index]>=max)
			{
				max = marks[index];
			}
			if(marks[index]<=min)
			{
				min = marks[index];
			}
			if(marks[index]>=35)
			{
				failCount++;
			}
			
			total = total + marks[index];
			
			
			
		}
		average = total / marks.length;
		
		System.out.println("The Maximum Mark is " + max);
		System.out.println("The Minimum Mark is " + min);
		System.out.println("The Total is " + total);
		System.out.println("The Average is "+average);
		
		if(average >= 90 && average <= 100)
		{
			System.out.println("The Grade is A+");
		}
		else if(average >= 80 && average < 90)
		{
			System.out.println("The Grade is A");
		}
		else if(average >= 70 && average < 80)
		{
			System.out.println("The Grade is B+");
		}
		else if(average >= 60 && average < 70)
		{
			System.out.println("The Grade is B");
		}
		else if(failCount != 0)
		{
			System.out.println("No Grade");
		}
		else
		{
			System.out.println("The Grade is C");
		}
		
		

	}

}

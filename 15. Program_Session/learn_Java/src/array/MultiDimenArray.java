package array;

public class MultiDimenArray 
{
	
	public static void main(String[] args) 
	{
		int[][] marks = { {90,25,89,10,18},
						  {45,25,89,100,34},
						  {90,35,89,70,58}};
		
		//Using While to print the 2d array elements
//		int row = 0;   
//		while(row<=2)
//		{
//			int col = 0;
//			while(col<=4)
//			{
//				System.out.print(marks[row][col]+" ");
//				col =col + 1;
//			}
//			row = row + 1;
//			System.out.println();
//			
//		}
		
		for (int row = 0; row < marks.length;row++)
		{
			for(int col = 0; col<marks[row].length;col++)
			{
				System.out.print(marks[row][col] + " ");
			}
			System.out.println();
		}
		

	}

}

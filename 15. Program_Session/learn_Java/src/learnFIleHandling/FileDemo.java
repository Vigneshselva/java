package learnFIleHandling;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileDemo 
{

	public static void main(String[] args) throws IOException 
	{
			
			FileDemo fd = new FileDemo();
			
			//fd.fileMethod();
			//fd.fileMethod2();
			//fd.readWriteFile();
			fd.bufferWriterDemo();
			
	}
	public void readWriteFile() throws IOException 
	{
		File note = new File("/home/karkuvel/Documents/Programming_Languages/java/15. Program_Session/learn_Java/src/learnFIleHandling/File_Handling/Praba");
		
		FileWriter pen = new FileWriter(note,true);
		
//		pen.write("wwe ");
//		pen.write("is ");
//		pen.write("Wrestling");
//		pen.flush();
//		pen.close();
		
		FileReader reader = new FileReader(note);
		
		int r = reader.read();
		
		while(r != -1)
		{
			System.out.println((char)r);
			r = reader.read();
		}
		
		
		
	}
	public void fileMethod() throws IOException
	{	
		System.out.print("Enter Your File Path:");
		
		Scanner sc = new Scanner(System.in);
		
		String path = sc.nextLine();
		//If we want to use File Handling then we need to create the object for file class and import it
		File ff = new File(path);	
		//1.createNewFile()- This Method is used to create File and return value is Boolean.
		System.out.println(ff.createNewFile());
		//2.canRead() - This Method is to find the file can read or not and the return value is Boolean 
		System.out.println(ff.canRead());
		//3.canWrite() - This Method is to find the file can write or not and the return value is boolean
		System.out.println(ff.canWrite());
		//4.getName() - This Method is used to print the name of the file or directory only not path.
		System.out.println(ff.getName());
		//5.getPath() - This method is used to print the path of the file or directory
		System.out.println(ff.getPath());
		//6.isFile()- This method is used to print is file or Not and the return Value is boolean
		System.out.println(ff.isFile());
		//7.isDirectory()- This method is used to print is folder or Not and the return Value is boolean
		System.out.println(ff.isDirectory());
		//8.isHidden() - This Method is used to print the file or folder is hidden or not.
		System.out.println(ff.isHidden());
		//9.delete() - This method is used t delete the file. 
		System.out.println(ff.delete());
	}
	public void fileMethod2()
	{
		File ff = new File("/home/karkuvel/Documents/Java Program");
		File[] files = ff.listFiles();
		
		//Print the file list using loop
//		for(int i = 0;i<files.length;i++)
//		{	
//			if(files[i].isFile())
//			System.out.println(files[i].getName());
//		}
		
		//Program to Print The .java file without using predefined method.
		
		for(int i = 0; i < files.length;i++)
		{
			String name = files[i].getName();
			int dot = name.lastIndexOf(".");
			if(dot > 0)
			{
				String extenstion = name.substring(dot);
				if(extenstion.equals(".class"))
				{
					System.out.println(files[i].getName());
				}
			}
			
		}
	}
	public void bufferWriterDemo() throws IOException
	{
		File note = new File("/home/karkuvel/Documents/Programming_Languages/java/15. Program_Session/learn_Java/src/learnFIleHandling/File_Handling/Praba");
		
		FileWriter pen = new FileWriter(note,true);
		
		BufferedWriter bw = new BufferedWriter(pen);
		
//		bw.write("Karkuvel Prabakaran");
//		bw.write("is Computer Science Student");
//		bw.flush();
//		bw.close();
		
		FileReader reader = new FileReader(note);
		
		BufferedReader br = new BufferedReader(reader);
		
		String st = br.readLine();
		
		int count = 0;
		
		while(st != null)
		{
			System.out.println(st);
			st = br.readLine();
			count++;
		}
		System.out.println("Count ======> "+count);
	}
	

}

package learnString;

public class StringBufferDemo 
{

	public static void main(String[] args) 
	{
		StringBufferDemo sbd = new StringBufferDemo();
		
		String[] str = {"a","b","c","d"};
		
//		sbd.stringBufferMethod(str);
//		sbd.stringMethod(str);
//		sbd.stringBufferMethodList();
//		sbd.stringBuilderMethodList();
//		sbd.ternary();
		sbd.series();

	}
	public void stringBufferMethod(String[] str)
	{
		StringBuffer sb = new StringBuffer("");
		
		for(String ch:str)
		{
			System.out.println(sb.hashCode());
			sb = sb.append(ch); //String Buffer Method
		}
		System.out.println(sb.hashCode());
		System.out.println(sb);
	}
	public void stringMethod(String[] str)
	{
		String sm = "";
		
		for(String ch:str)
		{
			System.out.println(sm.hashCode());
			sm = sm.concat(ch); //String Method
		}
		System.out.println(sm.hashCode());
		System.out.println(sm);
	}
	public void stringBufferMethodList()
	{
		StringBuffer sb = new StringBuffer("selvakumar");
		  sb.append("monish");
		  sb.append("sarathkumar");
		  System.out.println(sb);
		  sb.insert(2, "123");
		  System.out.println(sb);
		  sb.replace(2, 5, "");
		  System.out.println(sb);
		  System.out.println(sb.reverse());
	}
	public void stringBuilderMethodList()
	{
		StringBuilder sb = new StringBuilder("selvakumar");
		  sb.append("monish");
		  sb.append("sarathkumar");
		  System.out.println(sb);
		  sb.insert(2, "123");
		  System.out.println(sb);
		  sb.replace(2, 5, "");
		  System.out.println(sb);
		  System.out.println(sb.reverse());
	}
	public void ternary() 
	{
		int no1 = 40;
		int no2 = 50;
		
		int big = (no1>no2)?no1:no2;
		
		System.out.println(big);
		
	}
	public void series()
	{
		int firstNumber  = 1;
		
		int temp = 1;
		System.out.println(firstNumber);
		for(int i = 0;i<6;i++)
		{
			
			firstNumber =firstNumber + temp + 1;
			temp++;
			System.out.println(firstNumber);
		}
	}

}



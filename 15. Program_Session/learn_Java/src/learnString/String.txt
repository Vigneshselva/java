									String Literals

java.lang:
	The Class Belongs java.lang can use directly and don't need to import the package java.lang.
	
String:
	String are Immutable.
	
All Implemented Interfaces:
Serializable, CharSequence, Comparable<String>

The String class represents character strings. All string literals in Java programs, such as "abc", are implemented as instances of this class.

		String str = "abc";
		
		str is a object for String class
		
Strings are constant; their values cannot be changed after they are created. String buffers support mutable strings. Because String objects are immutable they can be shared. For example:

     String str = "abc";
 
is equivalent to:

     char data[] = {'a', 'b', 'c'};
     String str = new String(data);

Once we using the Non-primitive datatype String it will create object without new keyword
and it will save in SCP(String Constant Pool) memory slot. 

	System.identityHashCode() - To Find the address of the variable.
	
	if the string value are same then it will use the same address for the mulltiple objects.
	
	if use the String using new keyword it will store in heap memory.

String Methods
1. charAt(index value) - it is like array index if give 1 means it will give character from sequence of word.
2. length() - it is a method used to find the length of the string.
3. toString()- it is used to print the object value directly.println method call toString() method.The class have only one toString() Method.


By default all the class have object class as parent class.
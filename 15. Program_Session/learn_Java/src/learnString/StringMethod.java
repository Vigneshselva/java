package learnString;

public class StringMethod 
{

	public static void main(String[] args) 
	{
		String name = "Manoj";
		String name2 = new String("Manoj");
		System.out.println(name.charAt(0));
		System.out.println(name.equals(name2));
		//System.out.println(name == name2);
		System.out.println(name.compareTo(name2));
		//100-78 = 32 --> it will take the ASCII Value of name variable First letter and subract the ASCII Value of name2 First Variable letter 
		System.out.println(name.compareToIgnoreCase(name2));
		//it will act like compareTo() but ignore the case sensitive 
		name = name.concat("Kumar").concat("s");
		//it will used to concat the value
		System.out.println(name);
		System.out.println(name.equals(name2));
		System.out.println(name.equalsIgnoreCase(name2));
		System.out.println(name.hashCode());
		System.out.println(name2.hashCode());
		System.out.println(name.startsWith("man"));
		System.out.println("---> "+name.endsWith("oj"));
		
		String name3 ="monimsh";
	     // 0123456
	  System.out.println(name3.isEmpty());
	  System.out.println(name3.indexOf('m'));
	  System.out.println(name3.lastIndexOf('m'));
	  
	  String name4 = "viGnEsh";
	     //  0123456
	  
	  System.out.println(name4.substring(2));
	  System.out.println(name4.substring(2,5));
	  char[] ar = name4.toCharArray();
	  for(char ch:ar) {
	   System.out.print(ch+" ");
	  }
	  System.out.println("==============");
	  for(int i=ar.length-1;i>=0;i--) {
	   System.out.print(ar[i]+" ");
	  }
	  System.out.println("==============");
	  System.out.println("==============");
	  
	  System.out.println(name4.toLowerCase());
	  System.out.println(name4.toUpperCase());
	  
	  String name5 ="12/12/2023";
	  System.out.println("==>"+name5.trim()+"<==");
	  System.out.println("==>"+name5.strip()+"<==");
	  System.out.println("==>"+name5.stripLeading()+"<==");
	  System.out.println("==>"+name5.stripTrailing()+"<==");
	  System.out.println('\u0B85');
	  String[] str = name5.split("2", 3);
	  
	  for (String string : str) {
	   System.out.println(string);
	  }
	  
	  
	  
	  String date = String.join("-", "12","12","2023","dfadsj");
	  System.out.println(date);
		
		

	}

}

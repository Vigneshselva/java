package learnString;

public class StringDemo 
{
	 String name;
	 int age;
	 public StringDemo(String name,int age) 
	 {
		  this.name=name;
		  this.age=age;
	 }

	public String toString() 
	{
		  return this.name+" "+this.age;
	}

	public static void main(String[] args) 
	{
//		String s  = "abc";
//		System.out.println(System.identityHashCode(s)); // For Printing the Memory address
//		s  = "bcd";
//		System.out.println(System.identityHashCode(s));
		
//		String p1 = "India";
//		System.out.println(System.identityHashCode(p1));
//		String p2 = "India";
//		System.out.println(System.identityHashCode(p2));
//		String p3 = "India";
//		System.out.println(System.identityHashCode(p3));
//		String p4 = "India";
//		System.out.println(System.identityHashCode(p4));
//		String p5 = "India";
//		System.out.println(System.identityHashCode(p5));
		
//		String p1 = "Aus";
//		System.out.println(System.identityHashCode(p1));
//		String p2 = "India";
//		System.out.println(System.identityHashCode(p2));
//		String p3 = "India";
//		System.out.println(System.identityHashCode(p3));
//		String p4 = "India";
//		System.out.println(System.identityHashCode(p4));
//		String p5 = "India";
//		System.out.println(System.identityHashCode(p5));
//		
//		String p6 = new String("India");
//		System.out.println(System.identityHashCode(p6));
//		String p7 = new String("India");
//		System.out.println(System.identityHashCode(p7));
		
		// Below Program is used to print the String literals into single character
//		String name = "sarathkumar";
//		System.out.println(name.length());
//		for(int i = 0;i < name.length();i++)
//		{
//			System.out.println(name.charAt(i));
//		}
		
		//Below Program for Printing the character if is a and count the value
//		String name = "sarathkumar";
//		int count = 0;
//		System.out.println(name.length());
//		for(int i = 0;i < name.length();i++)
//		{	
//			if(name.charAt(i)=='a')
//			{
//				System.out.println(name.charAt(i));
//				count++;
//			}
//		}
//		System.out.println("The a count is "+count);
		
		  String name = new String("sarathkumar");
		  StringDemo sd = new StringDemo("selvakumar",20);
		  //sd.
		  System.out.println(name);
		  System.out.println(sd);
		
	}
	
		
		
	

}

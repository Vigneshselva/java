package learnString;

public class ToString 
{
	String name;
	int year;

	public ToString(String name, int year) 
	{
		this.name = name;
		this.year = year;
	}
	public String toString() 
	{
		return name + " "+"\n"+year;
	}

	public static void main(String[] args) 
	{
		String str = new String("Karkuvel");
		
		System.out.println(str);
		
		ToString str1 = new ToString("Prabakaran",2010);
		
		System.out.println(str1);
		

	}

}

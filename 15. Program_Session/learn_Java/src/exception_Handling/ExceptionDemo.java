package exception_Handling;

import java.util.Scanner; //Import Package for Scanner class usage

public class ExceptionDemo 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in); // Scanner class Object
		
		ExceptionDemo ed = new ExceptionDemo(); //Object Creation
		
		System.out.println("Enter Two Number:");
		
		int no1 = sc.nextInt(); //Get Input From User
		int no2 = sc.nextInt(); //Get Input From User
		
		//ed.divide(no1,no2); //Call Method
		//ed.subract(no1,no2); //Call Method
		
		ed.fromUse();
	}
	
	private void fromUse() {
	    Scanner sc = new Scanner(System.in);
	      System.out.println("Enter size :");
	      int size = sc.nextInt();
	      int[] ar = new int[size];
	      for (int i = 0; i < ar.length; i++) 
	      {
	       System.out.print("Enter value :");
	       int value = sc.nextInt();
	       ar[i] = value;
	       System.out.println();
	      }

	      for (int marks : ar) 
	      {
	       System.out.print(" "+marks);
	      }

	  }

	private void subract(int no1, int no2) //Method Defining
	{
		System.out.println(no1-no2); //Print Statement
		
	}

	private void divide(int no1, int no2) //Method Defining
	{
		
		try //Exception Possible area
		{
				System.out.println(no1/no2); //Print Statement
				
				Scanner sc = new Scanner(System.in);
				
				
				int[] array = new int[no1];
				for(int i = 0;i<array.length;i++)
				{
					System.out.println(array[i]);
				}
		}
		catch(ArithmeticException ae) //ae is the object of ArithmeticException //Exception Handling area
		{
				ae.printStackTrace(); //
				System.out.println("Check No2");
		}
		catch(NegativeArraySizeException nas) 
		{
		      System.out.println("Check no1");
	    }
		catch(Exception e)
		{
				System.out.println("Something Went Wrong");
		}
		finally 
		{
		      System.out.println("will run always");
		}
	}
		
}



package learnRegex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo 
{

	public static void main(String[] args) 
	{
		/*
		 * String input = "My mobile number is 9884010000"; //Pattern patternObj =
		 * Pattern.compile("[a-zA-z]"); //Pattern patternObj = Pattern.compile("[A-z]");
		 * //Pattern patternObj = Pattern.compile("[0123456789]"); //Pattern patternObj
		 * = Pattern.compile("[0-9]"); //Pattern patternObj =
		 * Pattern.compile("\\d{10}"); // The Pattern object is used to compile the
		 * pattern and return the object Matcher matcherObj = patternObj.matcher(input);
		 * // The Matcher object is used to match the input using pattern object
		 * while(matcherObj.find()) { System.out.println(matcherObj.group()); // it is
		 * used to group // System.out.println(matcherObj.start()); // it is used to
		 * print the start index // System.out.println(matcherObj.end()); // it is used
		 * to print the end index
		 * 
		 * }
		 */
		/*
		 * String password = "Chennai is the capital of TamilNadu"; Pattern patternObj =
		 * Pattern.compile("TamilNadu$"); Matcher matcherObj =
		 * patternObj.matcher(password); while(matcherObj.find()) {
		 * System.out.print(matcherObj.group()); }
		 */
		
//		String password = "Chennai1234@gmail.com";
//		//Pattern patternObj = Pattern.compile("[A-Za-z0-9]");
//		//Pattern patternObj = Pattern.compile("[^A-Za-z0-9]");
//		Matcher matcherObj = patternObj.matcher(password);
//		while(matcherObj.find())
//		{
//		   System.out.print(matcherObj.group());
//		}
//		String password = "Chennai is the capital city";
//		 //Pattern patternObj = Pattern.compile("\\s"); //space count
//		 //Pattern patternObj = Pattern.compile("\\S"); //space remove
//		 Matcher matcherObj = patternObj.matcher(password);
//		  int count = 0;
//		  while(matcherObj.find())
//		  {
//		   count++;
//		   System.out.print(matcherObj.group());
//		  }
//		  System.out.println(count);
//		String password = "Chennai 600042 is Velachery";
//		 //Pattern patternObj = Pattern.compile("\\D"); // Digits Remover
//		 Pattern patternObj = Pattern.compile("\\d"); // only consider Digits  
//		 Matcher matcherObj = patternObj.matcher(password);
//		  while(matcherObj.find())
//		  {
//		   System.out.print(matcherObj.group());
//		  }
//		String mobile = "9884012810";
//		Pattern patternObj = Pattern.compile("[6-9][0-9]{9}"); // Validate phone number
//		Matcher matcherObj = patternObj.matcher(mobile);
//		while(matcherObj.find())
//		{
//			System.out.print(matcherObj.group());
//		}
		String mobile = "919884012810";
		 Pattern patternObj = Pattern.compile("(0|91)?[6-9][0-9]{9}");
		  Matcher matcherObj = patternObj.matcher(mobile);
		  while(matcherObj.find())
		  {
		   System.out.print(matcherObj.group());
		  }
		
		

	}

}

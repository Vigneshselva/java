package conditional_Statement;

public class MaximumNumber 
{

	public static void main(String[] args) 
	{
		int no1 = 45;
		int no2 = 67;
		int no3 = 33;
		
		if(no1>no2)
		{
			if(no1>no3)
			{
				System.out.println("Number 1 is greater");
			}
			else if (no3 > no1)
			{
				System.out.println("Number 3 is greater");
			}
			else
			{
				System.out.println("Number 1 and Number 3 are same and is greater");
			}
		}
		
		else if (no2>no1)
		{
			if(no2>no3)
			{
				System.out.println("Number 2 is greater");
			}
			else if (no3>no2)
			{
				System.out.println("Number 3 is greater");
			}
			else
			{
				System.out.println("Number 2 and Number 3 are same and is greater");
			}
		}
		else 
		{
			if (no1 == no2) 
			{
				if(no1>no3)
				{
					System.out.println("Number 1 and Number 2 are same and is greater");
				}
				else if (no3 > no1)
				{
					System.out.println("Number 3 is greater");
				}
				else
				{
					System.out.println("All are same number");
				}
			}
		}
		

	}

}

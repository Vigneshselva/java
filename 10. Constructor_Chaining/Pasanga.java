public class Pasanga extends AmmaAppa
{
	public Pasanga()//no-args Constructor
	{
		
		System.out.println("Pasanga no-args constructor ");
	}
	public Pasanga(int no)
	{
		//super()
		//new AmmaAppa() is equal to super()
		// implicit call - super() is invisible in the constructor
		// explicit constructor is created by user 
		//super(10, true);
		System.out.println("Pasanga Single-agrs constructor");
		super(10, true);
		
	}
	public static void main (String [] args)
	{
		Pasanga pp = new Pasanga(30); // object creating
	}
}
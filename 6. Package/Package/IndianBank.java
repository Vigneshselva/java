package com.indianBank;  // Creating directory Or Folder by using package statement Package is used for code reuseablity

/*Import the class InterestCalculator . It is a userdefined package . If the class in same package then
no need to import the class or else it in different package then we need to import the package with class name*/

import com.indianBank.loan.InterestCalculator;

public class IndianBank
{
	public static void main(String [] args)
	{
		InterestCalculator ic = new InterestCalculator();
		
		ic.calculate();
	}
}
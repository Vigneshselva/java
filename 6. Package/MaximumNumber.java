package com.javaProgram; // creating package with directory

public class MaximumNumber
{
	int no1; // Declare instance variable
	int no2; // Declare instance variable
	int no3; // Declare instance variable
	
	//Constructor use to initialize the value for object
	public MaximumNumber(int no1 , int no2 , int no3) // Constructor with three aruguments
	{
		this.no1 = no1; // this keyword refers to current object
		this.no2 = no2;
		this.no3 = no3;
	}
	public static void main(String [] args)
	{
		MaximumNumber object = new MaximumNumber(22,12,45); // creating object with three parameters
		
		object.maxNumber(); // calling method
	}
	public void maxNumber() // method defining
	{
		/*
											Algorithm		
					1. Take the three numbers as input and store them in variables.
					2. Check the first number if it is greater than other two.
					3. Repeat the step 2 for other two numbers.
					4. Print the number which is greater among all and exit.
		*/
		
		if(no1>=no2 && no1>=no3) // using if statement to check no1 is greater than or equal to no2 and no3 then print no1 is greater
		{
			System.out.println("The Greatest Number is "+no1);
		}
		else if(no2>=no1 && no2>=no3) // using else if statement to check no2 is greater than or equal to no1 and no3 then print no2 is greater
		{
			System.out.println("The Greatest Number is "+no2);
		}
		else // if above two condintion became false then use else to print the no3 is greater 
		{
			System.out.println("The Greatest Number is "+no3);
		}
	}
}
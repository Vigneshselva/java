package chennai; // Create a directory or folder using package

public class Child_Class_2 extends Parent // Sub-Class or Derived class inherites Super class or Parent class(Parent) using extends key word
{
	public static void main(String [] args)
	{
		Child_Class_2 cc = new Child_Class_2();  // Object Creating for sub class 
		
		cc.speaking(); // Sub class object call super class or parent class (Parent) method
		
		cc.walking(); // Sub class object call super class or parent class (Parent) method
	}
}
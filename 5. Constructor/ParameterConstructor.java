public class ParameterConstructor
{
	String name; // instance object
	
	
	public ParameterConstructor(String name) // Constructor with arguments
	{
		//this keyword is refers to current object
		this.name = name;
	}
	public static void main(String [] args)
	{
		// creating object with parameter for constructor to initialize the vale for object using constructor
		ParameterConstructor object = new ParameterConstructor("Praba"); 
		
		System.out.println(object.name); // printing statement
		
	}
}
											Constructor
											
what is Constructor ?
	
	A constructor is a special type of method that is used to initialize objects of a class. 
Constructors are called when an object of a class is created. 
They have the same name as the class and do not have a return type.

constructor
	 1) Should have same class name 
	 2) No return data type required
	 3) call automatically when object is created 
	 4) used to intializing object specific values
	
this keyword
	1) this keyword refer to current object
	2) this keyword cannot be used in static

Types Of Constructor
1.Default Constructor
2.Parameterized Constructor
3.Constructor Overloading

Methods:

    Purpose: Methods are used to define the behavior or actions of objects. They perform some action, return a value, or manipulate the object's state.

    Name: Methods have a name that is chosen by the programmer, and they can have a return type (including void for methods that don't return a value).

    Invocation: Methods are called explicitly by name and can be invoked at any time after an object is created.

    Overloading: Methods can also be overloaded, allowing you to define multiple methods with the same name but different parameter lists.

    Explicit return: Methods can have a return statement that specifies the type of value they return. Methods with a return type must return a value of that type.

    Accessibility: Methods can have access modifiers and can also be marked as static (belonging to the class, not an instance) or final (cannot be overridden in subclasses).

Examples:

Constructor example:

java

public class MyClass {
    public MyClass() {
        // Constructor code
    }
}

MyClass obj = new MyClass(); // Constructor is called when creating an object


Method example:

java

public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }
}
Calculator calc = new Calculator();
int result = calc.add(5, 3); // Method is called to perform an addition



1. Default Constructor

public class DefaultConstructor
{
	/*
		Default Constructor - when object creates and it call constructor default so java have already
		perdefined constructor it is know as default constructor
	*/
	public static void main(String [] args)
	{
		
		
		DefaultConstructor object = new DefaultConstructor(); // creating object
	
		// object call constructor with o arguments
		
	}
}


2. No-arg Constructor

public class NoargConstructor
{
	/*
		No-argConstructor - If we create a contructor with no-arguments it is called no-arg constructor 
		beacause the default constructor is overloaded so it is named as no-arg constructor.
	*/
	
	//No-arg Constructor
	public NoargConstructor()
	{
	
	}
	
	
	public static void main(String [] args)
	{
		NoargConstructor object = new NoargConstructor();
	}
}

3. Parameter Constructor

public class ParameterConstructor
{
	String name; // instance object
	
	
	public ParameterConstructor(String name) // Constructor with arguments
	{
		//this keyword is refers to current object
		this.name = name;
	}
	public static void main(String [] args)
	{
		// creating object with parameter for constructor to initialize the vale for object using constructor
		ParameterConstructor object = new ParameterConstructor("Praba"); 
		
		System.out.println(object.name); // printing statement
		
	}
}


4. Constructor Overloading

public class ConstructorOverload
{
	/*
		Constructor Overloading - the Constructor with same name with different type of aruguments or
		different number of arguments
	*/
	
	String name; // declare instance variable
	int age; // declare instance variable
	
	public ConstructorOverload(String name , int age)
	{
		this.name = name; //this keyword refers to current object
		this.age = age; //this keyword refers to current object
		
	
	}
	
	public ConstructorOverload(String name )
	{
		this.name = name; //this keyword refers to current object
	}
	
	public static void main(String [] args)
	{
		//Object creating with parameters and it call constructor to assign value to object
		ConstructorOverload object = new ConstructorOverload("Praba",22); 
		
		//Object creating with parameters and it call constructor to assign value to object
		ConstructorOverload object1 = new ConstructorOverload("karkuvel");
		
		System.out.println("Name: "+object.name+"\nAge: " + object.age+" =====> Object Output");//Printing statement
		
		System.out.println("Name: "+object1.name+" =====> Object 1 Output");//Printing Statement
		
	}
}

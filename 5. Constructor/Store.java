public class Store
{
	static String store_Name; // Declare static variable
	
	static String address; // Declare static variable 
	
	String product_Name; // Declare Non-static variable 
	
	int mrp; // Declare Non-static variable
	
	public Store(String product_Name, int mrp) // constructor 
	{
		this.product_Name = product_Name; // this keyword refer to current object
		this.mrp = mrp; // this keyword refer to current object
	}
	
	public static void main(String [] args)
	{
		// static String name; it will throw error because static variable cannot declare in the method
		
		Store.store_Name = "Vasuki Stores";// intialize the static variable using class name
		
		//Local Variable
		String name = Store.store_Name;  // Static variable can be assign to the local variable
										// But Instance Variable are can not assign to local variable

			
		Store product1 = new Store("Shampo",256); // object creating
		
		Store product2 = new Store("Dal",315); // object creating
		
		Store product3 = new Store("Rice",1800); // object creating
		
		Store product4 = new Store("Potato",55); // object creating
		
		Store product5 = new Store("Onion",45); // object creating
		
		product1.product_details(); // method calling
		product2.product_details(); // method calling
		product3.product_details(); // method calling
		product4.product_details(); // method calling
		product5.product_details(); // method calling
		
		/*
		public void product_details()  //It will throw error becaeuse Method Cannot be Defining 
		{												inside the another Method
		
		}
		*/
		
	}
	
	public void product_details() // Method Defining
	{
		System.out.println("Product Name : "+product_Name+"\nMRP: "+mrp); // Printing Statement
		
		
	}
	
}
public class NoargConstructor
{
	/*
		No-argConstructor - If we create a contructor with no-arguments it is called no-arg constructor 
		beacause the default constructor is overloaded so it is named as no-arg constructor.
	*/
	
	//No-arg Constructor
	public NoargConstructor()
	{
	
	}
	
	
	public static void main(String [] args)
	{
		NoargConstructor object = new NoargConstructor();
	}
}
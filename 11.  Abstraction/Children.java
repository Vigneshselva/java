public abstract class Children extends Parents // Abstract Class
{
	public static void main(String [] args)
	{
		//Children child = new Children();
		
		//child.study();
		
		//Parents.motivate(); static method can call even if a abstract class
		
	}
	// Children is abstract; can not be instantiated
	// instantiated - instance
	//public abstract void study();
	public abstract void mbbs(); // Abstract Method
	
	public void play()
	{
		System.out.println("Playing Cricket");
	}

}
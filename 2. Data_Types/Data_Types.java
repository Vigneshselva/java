public class Data_Types
{
	public static void main(String [] args)
	{
		/*
			Primitive Data Types
			1. byte
			2. short
			3. int
			4. long
			5. float
			6. double
			7. char
			8. boolean
		*/
		byte door_no = 23;	// byte --> 1 byte or 8 bits  
		short total_Marks = 256;	// short --> 2 byte or 16 bits 
		int roll_No = 411404006;	// int --> 4 byte or 32 bits
		long mobile_No = 6379271473L;	// long --> 8 byte or 64 bits 
		float pi_Value = 3.14F;	// float --> 4 byte or 32 bits		
		double root_Value = 1.41421356; // double --> 8 byte or 64 bits
		char initial = 'p';	// char --> 2 byte or 16 bits
		boolean isAddress = true;	// boolean --> 1 byte or 8 bits
		
		System.out.println("The Door no is "+door_no);
		System.out.println("The Total Marks is "+total_Marks);
		System.out.println("The Roll no is "+roll_No);
		System.out.println("The Mobile no is "+mobile_No);
		System.out.println("The value of Pi is "+pi_Value);
		System.out.println("The Square root of 2 is "+root_Value);
		System.out.println("The initial is "+initial);
		System.out.println("Is Address is vaild ? "+isAddress);
	}
}
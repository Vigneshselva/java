public class Trainer 
{
	//instance variable
	String dept = "Java";
	String institue = "Payilagam";
	
	//Private Instance Variable
	private int salary = 10000;
	
	public int getSalary() //Getter Method for salary
	{
		return salary;
	}
	
	public Trainer(String dept , String institue) // Parameterised Constructor Overloaded
	{
		this.dept = dept; // this refers to current object 
		this.institue = institue; // this refers to current object 
	}
	
	public Trainer() // No-args Constructor
	{
		
	}
	
	public static void main(String [] args)
	{
		Trainer trainKumar = new Trainer("CSE","Payilagam"); // object creating with arguments
		
	}
	public void training() //Method Defining
	{
		System.out.println("Training"); 
	}
	
}
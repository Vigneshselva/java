public class SQLTrainer extends Trainer // SQLTrainer class inherites the Trainer (Sub class inherites the super class)
{
	
	public static void main(String [] args)
	{
		SQLTrainer ram = new SQLTrainer(); // object creating for sub class but same for super class also
		
		System.out.println(ram.dept); // access the super class instance variable
		System.out.println(ram.institue); // access the super class instance variable
		
		ram.training(); // call super class instance method
		
		System.out.println(ram.getSalary()); // use getter method to see the value of salary because salary is in private
		
	}
}
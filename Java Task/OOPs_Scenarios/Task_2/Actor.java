public interface Actor
{
	boolean makeUpRequired = true; // In interface the variable is in default Final 
	
	String address = "Chennai"; // In interface the variable is in default Final 
	
	public void act(); // In interface the Method is in default Abstaract
	
	public void dance(); // In interface the Method is in default Abstaract
	
	public void sing(); // In interface the Method is in default Abstaract
}
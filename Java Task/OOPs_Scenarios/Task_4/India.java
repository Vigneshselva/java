public abstract class India
{
	static String capital = "New Delhi"; // static variable
	
	public India(String primeMinister) // parameter constructor
	{
		System.out.println("Our Prime Minister is "+primeMinister);
	}
	
	public India()
	{
		
	}
	
	abstract void speakLanguage();// abstract method
	
	abstract void eat(); // abstract method
	
	abstract void dress(); // abstract method
	
	
}
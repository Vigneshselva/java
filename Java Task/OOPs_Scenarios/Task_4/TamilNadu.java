public class TamilNadu extends SouthIndia
{
	static String capital = "Chennai"; // static variable
	
	public static void main(String [] args)
	{
		System.out.println(India.capital); // call static variable using class name
		
		System.out.println(TamilNadu.capital); // call static variable using class name
		
		SouthIndia s1 = new TamilNadu(); // Dynamic Binding
		
		// call method using dynamic binding object 
		s1.speakLanguage(); 
		s1.eat();
		s1.dress();
		s1.cultivate();
		s1.livingStyle();
		
	
	}
	
	void speakLanguage() //Abstract method defined
	{
		System.out.println("Tamil");
	}
	
	void eat() //Abstract method defined
	{
		System.out.println("Idly");
	}
	
	void dress() //Abstract method defined
	{
		System.out.println("Westi-Shirt");
	}
	
	void cultivate() //Abstract method defined
	{
		System.out.println("Rice and Sugar cane cultivation");
	}
	
	void livingStyle() //Abstract method defined
	{
		System.out.println("Above Average Development");
	}
}
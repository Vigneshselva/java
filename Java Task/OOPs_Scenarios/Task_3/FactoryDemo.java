public abstract class FactoryDemo extends SmartPhone // The class extends the smartphone
{
	boolean isOriginalPiece = false; //instance variable 
	
	static int price = 0; // static variable
	
	abstract void verifyFingerPrint(); // abstract method  
	
	abstract void providePattern(); // abstract method  
	
	void browse() // method defining
	{
		System.out.println("FactoryDemo Browsing");	
	}
}
public class Samsung extends FactoryDemo 
{
	static int price = 5000; // static variable
	
	public static void main(String [] args)
	{
		Samsung sam = new Samsung(); // object creating 
		
		sam.browse(); // method calling
		
		System.out.println(sam.price); 
		
		// sam.browse() is print the value of factory browsing because super class of samsung is factorydemo so its pirority to print the super class value
		
	}
	
	int call(int seconds)
	{
		return seconds;
	}
	
	void sendMessage()
	{
		System.out.println("Sending Message");
	}
	
	void receiveCall()
	{
		System.out.println("Recieving Call");
	}
	
	void verifyFingerPrint()
	{
		System.out.println("Fingerprint");
	}
	
	void providePattern()
	{
		System.out.println("Pattern");
	}
}
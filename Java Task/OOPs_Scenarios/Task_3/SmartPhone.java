public abstract class SmartPhone
{
	public SmartPhone() // no-args constructor
	{
		System.out.println("SmartPhone Under Development");
	}
	
	abstract int call(int seconds); // Abstract method it was defined by sub class
	
	abstract void sendMessage(); // Abstract method it was defined by sub class
	
	abstract void receiveCall(); // Abstract method it was defined by sub class
	
	void browse() // method defining
	{
		System.out.println("SmartPhone Browsing");	
	}
}
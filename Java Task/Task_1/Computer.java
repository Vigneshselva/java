public class Computer // Class 
{
	// class variable
	static String ram;
	static String processor;
	
	//object variable 
	int price;
	String os; 
	String touchOrNormalScreen;
	
	//Constructor
	public Computer(int price,String os,String touchOrNormalScreen)
	{
		this.price = price;
		this.os = os; 
		this.touchOrNormalScreen = touchOrNormalScreen ;
		
	}
	
	public static void main (String [] args) // main method
	{
		//object creating 
		Computer dell = new Computer(50000,"Windows 10","Normal Screen");
		Computer lenovo = new Computer(5500,"Windows 11","Touch Screen");
		
		dell.details("Dell");
		lenovo.details("Lenovo");
		
		
	}
	public void details(String name) // method defining
	{
		
		System.out.println(name);

	
		System.out.println("\nPrice = "+price +"\n OS = "+os+"\nTouch Or Normall Screen = "+ touchOrNormalScreen);
		System.out.println("\n");
		
		
	}
}
public class Hotel
{
	private int bill = 123;
	
	public int getBill()
	{
		return this.bill;
	}
	public void setBill(int bill)
	{
		if(bill >= this.bill)
			this.bill = bill;
	}
}
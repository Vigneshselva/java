public class SuperMarket
{
	//Declare And Initialize the Static Variable or class variable
	static String shopName = "Monish";
	static int doorNo = 5;
	
	//Declare And Initialize the Non-Static variable or Instance Variable
	String name;
	int price;
	
	public static void main(String [] args)
	{
		SuperMarket product1 = new SuperMarket();// object creating
		
		//intialize the value using the object
		product1.name = "Soap";
		product1.price = 30;
		
		//printing the object variable using object
		System.out.println(product1.name);
		System.out.println(product1.price);
		
		SuperMarket product2 = new SuperMarket(); //object creating
		
		//intialize the value using the object
		product2.name = "Dal";
		product2.price = 50;
		
		//printing the object variable using object
		System.out.println(product2.name);
		System.out.println(product2.price);
		
		product1.buy();//Method Calling Statement
		
	}
	public void buy() //Method definition 
	{
		System.out.println("Buy Method Call Succesfully");	
	}
	

}
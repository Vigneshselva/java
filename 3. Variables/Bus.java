public class Bus
{
	// Declare class Variable
	static String bus_Name = "Chennai Travellers";
	static String bus_No = "TN-07-1121";
	
	//Declare object Variable
	String driver_Name = "Praba";
	String conductor_Name = "Ravi";
	String departure = "Kovilpatti";
	String destination = "Chennai";
	int seats = 40;
	int seats_Remaining = 06;
	public static void main(String [] args)
	{
		Bus object = new Bus(); // object creating
		
		//Printing class variable
		System.out.println("Bus Name : "+ Bus.bus_Name);
		System.out.println("Bus No : "+ Bus.bus_No);
		
		//Printing object variable
		System.out.println("Driver Name: "+object.driver_Name);
		System.out.println("Conductor Name: "+ object.conductor_Name);
		System.out.println("Departure From "+ object.departure);
		System.out.println("Destination is "+ object.destination);
		System.out.println("Total Seats is " + object.seats);
		System.out.println("Remaining Seats is "+ object.seats_Remaining);
		
	}
}
								Variables

what is variables?

Variables are containers for storing data values. 

Rules for variables

1. A variable name can consist of Capital letters A-Z, lowercase letters a-z digits 0-9, and two special characters such as _ underscore and $ dollar sign.
2. The first character must not be a digit.
3. Blank spaces cannot be used in variable names.
4. Java keywords cannot be used as variable names.
5. Variable names are case-sensitive.
6. There is no limit on the length of a variable name but by convention, it should be between 4 to 15 chars.
7. Variable names always should exist on the left-hand side of assignment operators.
8. First letter should start in small letter (IMPORTANT RULE)

Types of Variables 

1. Static Variables
2. Instance Variables
3. Local Variables

global variable OR field                   local variables
1. Static Variables
2. Instance Variables

1.Static Variables
A class variable or static Variables is any field declared with the static modifier; 
this tells the compiler that there is exactly one copy of this variable 
in existence, regardless of how many times the class has been instantiated.

Example

public class Variables
{
    static int age = 26; // Declare Static or Global Variables (class Variable)
	
	public static void main()
	{
		/* 
		Static or Global Variables (class Variable) can not be declare in method it only declare in class.

		*/	
		System.out.println(Variables.age); // Printing the static variable using class name with dot operator
	}
}

2. Local Variables

A variable declared inside the body of the method is called local variable. 
You can use this variable only within that method and the other methods in the class aren't even aware that the variable exists.

A local variable cannot be defined with "static" keyword.

Example

public class Variables
{
    public static void main()
	{
		int age = 26; // Declare Local Variables 
		/* 
		A local variable cannot be defined with "static" keyword. 
		*/	
		System.out.println(age); // Printing the local variable
	}
}

3.Instance Variables(object variable) // Field or Global variable
objects store their individual states in "non-static fields", that is, 
fields declared without the static keyword. 
Non-static fields are also known as instance variables because their values are unique to each instance of a class.  

Example
public class Instance_Variables 
{
	String name = "Praba"; //Declare Non-Static or Global Variables (object Variable)
	int age = 23; //Declare Non-Static or Global Variables (object Variable)

	public static void main(String [] args)
	{
		 
		Instance_Variables object = new Instance_Variables();//object creating
		
		
		name = object.name;// Assigning the Non-static variable value to local variable
		age = object.age;
		
		// printing local variables
		System.out.println("The Name is " +name);
		System.out.println("The Age is " +age); 
		
		//printing instance varibles using object
		System.out.println("The Name is " +object.name);
		System.out.println("The Age is " +object.age);
	}
}

Static Variable or class variable or field
When a variable is declared as static, then a single copy of the variable is created and shared among all objects at a class level. 

Instance Variable or object variable or field
Non - static variable multiple memory copy are maintain.

How to call static variable

1. we can use the static variables by using Class Name.
2. we can use the static variables by using Object Name.
3. we can call directly if u are in same class.



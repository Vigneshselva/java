public class Mobile
{
	//intialize the static variable or class variable
	static String storeName = "POCO";
	
	//declare the static variable or class variable
	static String storeAddress;
	
	//intialize the non-static or instance or object variable 
	String phoneName = "POCO M2 PRO";
	String price = "12,000";
	
	//declare the non-static or instance or object variable 
	String phoneName1;
	String price1;
	
	public static void main(String [] args)
	{
		//print the static variable using class name 
		System.out.println("The Store Name is "+Mobile.storeName);
		
		//initialize the static variable using class name in method
		Mobile.storeAddress = "Karapakkam";
		
		//print the static variable using class name 
		System.out.println("The Store Place is "+Mobile.storeAddress);
		
		Mobile mobile = new Mobile();//object creating 
		
		//print the non-static variable using object
		System.out.println("The Phone Name is "+mobile.phoneName);
		System.out.println("The Phone Price is "+mobile.price);
		
		Mobile mobile1 = new Mobile();//object ceating
		
		//initialize the value for non-static variable using object name in the method
		mobile1.phoneName1 = "POCO M3 PRO";
		mobile1.price1 = "15,000";
		
		//print the non-static by using object
		System.out.println("The Phone Name is "+mobile1.phoneName1);
		System.out.println("The Phone Price is "+mobile1.price1);
		
		Mobile mobile2 = new Mobile();// object creating
		
		//initialize the value for non-static variable using object name in the method
		mobile2.phoneName1 = "POCO M4 PRO";
		mobile2.price1 = "17,000";
		
		//print the non-static by using object
		System.out.println("The Phone Name is "+mobile2.phoneName1);
		System.out.println("The Phone Price is "+mobile2.price1);
		
	}
}
//local declare in the  method , constructor , block.
//best way to call the static variable by using class name.
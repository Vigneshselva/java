public class Calculator
{
	public static void main(String [] args)
	{
		Calculator cc = new Calculator(); // Object Creating
		
		 float result = cc.add(); // Calling Method By using object and store it in variable result
		
		// System.out.println(result); // printing the result
		
		//System.out.println(cc.add()); //Directly printing the Method call value
		
		cc.divide(result);
		cc.divide(result,5);
	
	}
	public float add() // Method Defining with using int return type 
	{
		//int no1 = 10;                /* Commented Because unwanted Variable consume 
		//int no2 = 15;					 memory so don't use unwanted varilable in program*/			
		//int total = no1 + no2;
		return 100 + 150.5f;
	}
	public void divide()
	{
		System.out.println("No Arguments");
	}
	public void divide(float re)
	{
		System.out.println(re/10);
	}
	public void divide(String str)
	{
		System.out.println(str);
	}
	public void divide(float re , int no)
	{
		System.out.println(re/no);
	}
	public int add(int a, int b)
	{
		return a + b;
	}
	// this part show error because the arugument in the method is same to previous method (int , int) so if change the return type also it show error
	/*public int add(int no1, int no2)
	{
		return no1 + no2;
	}*/
}
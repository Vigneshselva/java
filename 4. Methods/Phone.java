public class Phone 
{
	public static void main(String[] args)
	{
		Phone person = new Phone(); // Object Creating
		
		person.call(); // Method Calling By using object name.
		
		person.message(); // Method Calling By using object name.
		
	}
	
	public void call() // Method Defining
	{
	 	System.out.println("Call Method is Sucessfully Executed"); // Printing Statement	
		
	}
	
	public void message() // Method Defining
	{
		Phone person_typing = new Phone(); // Object Creating
		
		person_typing.typping(); // Method calling another method by using object
	}
	
	public void typping() // Method Defining
	{
		System.out.println("Message Method is Sucessfully Executed"); // Printing Statement
	}
	
}